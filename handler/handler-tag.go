package handler

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/zenyuk_go/articles-and-tags/model"
)

// TODO: temp. in memory storage
var tagDetails = make(map[string]model.TagDetails)

func init() {
	tagDetails["health2016-09-22"] = model.TagDetails{
		Tag:           "health",
		Count:         17,
		ArticleIDs:    []uint64{1, 7},
		RelatedTagIDs: []string{"science", "fitness"},
	}
}

// GetTagRelatedData returns list of Article IDs by tag name and date
func GetTagRelatedData(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	tagName, ok1 := vars["tagName"]
	date, ok2 := vars["date"]
	if !ok1 || !ok2 {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}
	detail, ok := tagDetails[tagName+date]
	if !ok {
		http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	encoder.Encode(detail)
}
