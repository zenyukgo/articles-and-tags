# Start
To start the service please run `./articles-and-tags` on Mac. For Windows run `articles-and-tags.exe`

# Initialize
To initialize articles repository please run the Curl script

`curl --header "Content-Type: application/json"   --request POST   --data '{"id": 1,"title": "latest science shows that potato chips are better for you than sugar","date": "2016-09-22","body": "some text, potentially containing simple markup about how potato chips are great","tags": ["health", "fitness", "science"]}'   http://localhost:80/articles`

# Query
Open Web Browser and navigate to http://localhost:80/articles/1 to see the acticle you inserted in step Initialize.
To test tag details (links to articles) call, navigate to http://localhost/tag/health/2016-09-22

*Assumption was made that persistance layer is out of scope of the task, hence implemented as a in-memory stub data, which can be updated as well.*
