package main

import (
	"fmt"
	"net/http"

	"gitlab.com/zenyuk_go/articles-and-tags/handler"

	"github.com/gorilla/mux"
)

func main() {
	fmt.Println("Starting Articles and Tags service on port 80")
	r := mux.NewRouter()

	r.HandleFunc("/articles/{id}", handler.GetArticle).Methods("GET")
	r.HandleFunc("/articles", handler.CreateArticle).Methods("POST")

	r.HandleFunc("/tag/{tagName}/{date}", handler.GetTagRelatedData).Methods("GET")

	http.ListenAndServe(":80", r)
}
